import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    ImageBackground
} from 'react-native';

export default class createTask extends Component {
    static navigationOptions = {
        header : null
    }
    render() {
        return (
            <ImageBackground
                source={require('./images/bg2.jpg')}
                style={{ flex: 1 }}
            >
            <View style={styles.container}>
                <TouchableOpacity 
                    style={styles.ButtonStyle}
                    onPress={()=> this.props.navigation.navigate('PersonalTask')}
                    >
                    <Text style={styles.txt}>Personal Task</Text>
                </TouchableOpacity>

                <TouchableOpacity 
                    style={styles.ButtonStyle}
                    onPress={()=> this.props.navigation.navigate('ShareTask')}
                    >
                    <Text style={styles.txt}>Share Task</Text>
                </TouchableOpacity>

                <TouchableOpacity 
                    style={styles.ButtonStyle}
                    onPress={()=> this.props.navigation.navigate('Medication')}
                    >
                    <Text style={styles.txt}>Medication</Text>
                </TouchableOpacity>

                <TouchableOpacity 
                    style={styles.ButtonStyle}
                    onPress={()=> this.props.navigation.navigate('Habit')}
                    >
                    <Text style={styles.txt}>Build Habit</Text>
                </TouchableOpacity>

                <TouchableOpacity 
                    style={styles.ButtonStyle}
                    onPress={()=> this.props.navigation.navigate('Gathering')}
                    >
                    <Text style={styles.txt}>Gathering</Text>
                </TouchableOpacity>
            </View>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 35,
        paddingVertical: 100,
    },
    ButtonStyle: {
        flex: 1,
        backgroundColor: '#1d9d74',
        margin: 10,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
    },
    txt : {
        fontSize : 20,
        color : 'white'
    }
})