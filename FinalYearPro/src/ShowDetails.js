import React , {Component} from 'react';
import { 
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    ListView,
    ScrollView
 } from 'react-native';
 import firebase from '@firebase/app'
import database from '@firebase/database'

 export default class ShowDetails extends Component {

    constructor() {
        super();
        let ds = new ListView.DataSource({rowHasChanged : (r1,r2) => r1 !==r2});
        this.state = {
            itemDataSource : ds
        } 
        this.itemsRef = this.getRef().child('PersonalTasks')
        this.renderRow = this.renderRow.bind(this);
        this.pressRow = this.pressRow.bind(this);
    }
    static navigationOptions = {
        header: null
    }

    componentWillMount() {

        this.getItems(this.itemsRef);
    }

    componentDidMount() {
        this.getItems(this.itemsRef);
    }

    getItems(itemsRef) {
        //let items = [{title : 'Item One '} , {title : 'Item Two'}];

        itemsRef.on('value',(snap)=> {
            let items =[];
            snap.forEach((child) => {
                items.push({
                    title : child.val().repeat,
                    key : child.key,
                    repeat : child.val().title,
                    alarmType : child.val().alarmType,
                    followupBy : child.val().followupBy
                    
                })
            })
            this.setState({
                itemDataSource : this.state.itemDataSource.cloneWithRows(items)
            })
        });

    }

    getRef() {
        return firebase.database().ref();
    }

    pressRow(item) {
        console.log(item);
    }

    renderRow(item){
        return(
            <TouchableOpacity
                onPress={()=> {
                    this.pressRow(item);
                }}
            >   
                <View style={styles.li}>
                    <Text style={styles.liText}>{item.title}</Text>
                    <Text style={styles.liText}>{item.repeat}</Text>
                    <Text style={styles.liText}>{item.alarmType}</Text>
                    <Text style={styles.liText}>{item.followupBy}</Text>
                    <Text style={{color:'red'}}> {item.key} </Text>
                </View>
            </TouchableOpacity>
        )
    }

     render() {
         return(
             <ScrollView style={styles.container}>
                 <ListView 
                    dataSource = {this.state.itemDataSource}
                    renderRow = {this.renderRow}
                />
             </ScrollView>
         );
     }
 }

 const styles = StyleSheet.create({

    container : {
        flex : 1,
    },
    li : {
        backgroundColor :'#fff',
        borderBottomColor: '#eee',
        borderColor: 'transparent',
        borderWidth: 1,
        paddingLeft: 16,
        paddingTop: 14,
        paddingBottom: 16,
    },
    liText : {
        color: '#333',
        fontSize : 16
    },

 })