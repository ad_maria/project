import React, { Component } from 'react';
import {
    View,
    Text,
    TextInput,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    TouchableOpacity,
    Alert,
    Image,
    ImageBackground
} from 'react-native';
import firebase from '@firebase/app';
import auth from '@firebase/auth'

export default class register extends Component {
    constructor(props) {
        super(props)
        this.state = ({
            userName: '',
            Number: '',
            email: '',
            password: '',
            confirmPassword: '',
            errorMessage: null
        })
    }
    static navigationOptions = {
        header: null
    }

    registerUser = (email, password) => {
        if (this.state.password.length < 6) {
            Alert.alert("Please enter atleast 6 characters password")
            return;
        }
        if (this.state.password != this.state.confirmPassword) {
            Alert.alert("Password does not match !")
            return;
        }

        if (isNaN(this.state.Number)) {
            Alert.alert("Value is Not Number");
            return;
        }


        firebase.auth().createUserWithEmailAndPassword(email, password)
            .then(() => this.props.navigation.navigate('Drawer'))
            .catch(error => Alert.alert(error.toString()))
    }

    render() {
        return (
            <ImageBackground 
                source={require('./images/bg3.png')}
                style={styles.container}
            >
                <StatusBar barStyle="light-content" />

                <View style={styles.container}>
                    <View style={styles.logoContainer}>
                        <Image
                            style={{ width: 150, height: 150, borderRadius: 10, borderColor: '#1d9d74', borderWidth: 3 }}
                            source={require('./images/image1.png')}
                        />
                    </View>
                    <View style={styles.loginInputsCon}>
                        <TextInput style={styles.input}
                            onChangeText={(userName) => this.setState({ userName })}
                            value={this.state.userName}
                            placeholder="Username"
                            placeholderTextColor="rgba(255,255,255,0.8)"
                            keyboardType="email-address"
                            returnKeyType="next"
                            autoCorrect={false}
                            onSubmitEditing={() => this.refs.userNumber.focus()}
                            underlineColorAndroid="rgba(255,255,255,0.8)"

                        />
                        <TextInput style={styles.input}
                            onChangeText={(Number) => this.setState({ Number })}
                            value={this.state.Number}
                            placeholder="Number"
                            placeholderTextColor="rgba(255,255,255,0.8)"
                            keyboardType="email-address"
                            returnKeyType="next"
                            autoCorrect={false}
                            onSubmitEditing={() => this.refs.userEmail.focus()}
                            ref={"userNumber"}
                            underlineColorAndroid="rgba(255,255,255,0.8)"
                        />
                        <TextInput style={styles.input}
                            placeholder="Email id"
                            placeholderTextColor="rgba(255,255,255,0.8)"
                            keyboardType="email-address"
                            returnKeyType="next"
                            autoCorrect={false}
                            ref={"userEmail"}
                            onSubmitEditing={() => this.refs.textPassword.focus()}
                            underlineColorAndroid="rgba(255,255,255,0.8)"
                            onChangeText={(email) => this.setState({ email })}
                            value={this.state.email}
                        />
                        <TextInput style={styles.input}
                            placeholder="Password"
                            placeholderTextColor="rgba(255,255,255,0.8)"
                            returnKeyType="next"
                            autoCorrect={false}
                            secureTextEntry
                            ref={"textPassword"}
                            onSubmitEditing={() => this.refs.textConfirmPassword.focus()}
                            underlineColorAndroid="rgba(255,255,255,0.8)"
                            onChangeText={(password) => this.setState({ password })}
                            value={this.state.password}
                        />
                        <TextInput style={styles.input}
                            placeholder="Confirm Password"
                            placeholderTextColor="rgba(255,255,255,0.8)"
                            returnKeyType="go"
                            autoCorrect={false}
                            secureTextEntry
                            ref={"textConfirmPassword"}
                            underlineColorAndroid="rgba(255,255,255,0.8)"
                            onChangeText={(confirmPassword) => this.setState({ confirmPassword })}
                            value={this.state.confirmPassword}
                        />
                        <TouchableOpacity style={styles.buttonLogin}
                            onPress={() => this.registerUser(this.state.email, this.state.password)}
                        >
                            <Text style={styles.buttonText}>REGISTER</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column'
    },
    logoContainer: {
        flex: 4,
        alignItems: 'center',
        justifyContent: 'center'
    },
    loginInputsCon: {
        flex: 6,
        justifyContent: 'center',
        padding: 15
    },
    logo: {
        fontSize: 35,
        color: 'black',
        //color: 'rgba(50,205,50, 0.9)',
        opacity: 0.9
    },
    input: {
        height: 45,
        color: 'white',
        fontSize: 15,
        padding: 15,
        marginBottom: 10,
        marginHorizontal: 20,
        paddingHorizontal: 10
    },
    buttonLogin: {
        backgroundColor: '#1d9d74',
        borderRadius : 5,
        height: 40,
        marginHorizontal: 20,
        paddingVertical: 15,
        justifyContent: 'center',
        marginBottom: 20
    },
    buttonText: {
        textAlign: 'center',
        color: 'white',
        fontWeight: 'bold',
        fontSize: 17,
        letterSpacing: 2
    }


})