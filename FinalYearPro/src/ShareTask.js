import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    TextInput,
    ImageBackground
} from 'react-native';

export default class ShareTask extends Component {
    static navigationOptions = {
        header : null
    }
    render() {
        return (
            <ImageBackground
                source={require('./images/bg3.png')}
                style={{ flex: 1 }}
            >
            <View style={styles.container}>
                <View style={styles.DateTimeContainer}>
                    <View style={styles.dateContainer}>
                        <TextInput style={styles.input}
                            placeholder="Date"
                            placeholderTextColor="rgba(255,255,255,0.8)"
                            underlineColorAndroid ="rgba(255,255,255,0.8)"
                            keyboardType="default"
                            autoCorrect={false}
                            returnKeyType="next"
                            onSubmitEditing={() => this.refs.Time.focus()}
                        />
                    </View>
                    <View style={styles.timeContainer}>
                        <TextInput style={styles.input}
                            placeholder="Time"
                            placeholderTextColor="rgba(255,255,255,0.8)"
                            underlineColorAndroid ="rgba(255,255,255,0.8)"
                            keyboardType="default"
                            autoCorrect={false}
                            returnKeyType="next"
                            ref={"Time"}
                            onSubmitEditing={() => this.refs.Title.focus()}
                        />
                    </View>
                </View>


                <View style={styles.inputs}>
                    <TextInput style={styles.input}
                        placeholder="Title"
                        placeholderTextColor="rgba(255,255,255,0.8)"
                        underlineColorAndroid ="rgba(255,255,255,0.8)"
                        keyboardType="default"
                        autoCorrect={false}
                        returnKeyType="next"
                        ref={"Title"}
                        onSubmitEditing={() => this.refs.Repeat.focus()}
                    />
                    <TextInput style={styles.input}
                        placeholder="Repeat"
                        placeholderTextColor="rgba(255,255,255,0.8)"
                        underlineColorAndroid ="rgba(255,255,255,0.8)"
                        keyboardType="default"
                        autoCorrect={false}
                        returnKeyType="next"
                        ref={"Repeat"}
                        onSubmitEditing={() => this.refs.AlarmType.focus()}
                    />
                    <TextInput style={styles.input}
                        placeholder="AlarmType"
                        placeholderTextColor="rgba(255,255,255,0.8)"
                        underlineColorAndroid ="rgba(255,255,255,0.8)"
                        keyboardType="default"
                        autoCorrect={false}
                        returnKeyType="next"
                        ref={"AlarmType"}
                        onSubmitEditing={() => this.refs.FollowupBy.focus()}
                    />
                    <TextInput style={styles.input}
                        placeholder="FollowupBy"
                        placeholderTextColor="rgba(255,255,255,0.8)"
                        underlineColorAndroid ="rgba(255,255,255,0.8)"
                        keyboardType="default"
                        autoCorrect={false}
                        returnKeyType="next"
                        ref={"FollowupBy"}
                        onSubmitEditing={() => this.refs.Attachments.focus()}
                    />
                    <TextInput style={[styles.input,styles.Attachments]}
                        placeholder="Attachments"
                        placeholderTextColor="rgba(255,255,255,0.8)"
                        underlineColorAndroid ="rgba(255,255,255,0.8)"
                        keyboardType="default"
                        autoCorrect={false}
                        returnKeyType="go"
                        ref={"Attachments"}
                    />
                    <TouchableOpacity style={styles.buttonLogin}
                    >
                        <Text style={styles.buttonText}>DONE</Text>
                    </TouchableOpacity>
                </View>
            </View>
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 50,
        paddingTop: 0,
        justifyContent: 'center'
    },
    DateTimeContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-end',
    },
    dateContainer: {
        flex: 1,
        marginRight: 10,
    },
    timeContainer: {
        flex: 0.5,
        marginLeft: 10,
    },
    inputs: {
        paddingTop : 10,
        flex: 2,
        flexDirection: 'column',
    },
    Attachments : {
        marginTop : 20
    },
    buttonLogin : {
        backgroundColor : '#1d9d74',
        borderRadius: 5,
        height: 40 ,
        paddingVertical : 15,
        justifyContent : 'center',
        marginTop: 20
    },
    buttonText : {
        textAlign : 'center',
        color : 'white',
        fontWeight : 'bold',
        fontSize: 17,
        letterSpacing: 2
    },
    input: {
        height: 50,
        color: 'black',
        fontSize : 20,
        paddingVertical: 15
    }
})