import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    TextInput,
    ImageBackground
} from 'react-native';
import firebase from '@firebase/app'
import database from '@firebase/database';
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment';

export default class Medication extends Component {
        constructor(props) {
            super(props);
            this.state = ({
                chosenDate: '',
                chosenTime: '',
                title: '',
                medicineName : '',
                chosenMedicineTime : '',
                repeat: '',
                quantity : '',
                chosenUpdateTill : '',
                alarmType: '',
                chosenfollowupBy: '',
    
                isDateVisible: false,
                isTimeVisible: false,
                isMedTimeVisible :false,
                isUpdateTillVisible : false,
                isFollowUpVisible: false
    
            })
        }
    
    
        static navigationOptions = {
            header: null
        }
    
        
        _showDateTimePicker = () => this.setState({ isDateVisible: true });
    
        _hideDateTimePicker = () => this.setState({ isDateVisible: false });
    
        _handleDatePicked = (date) => {
            console.log('A date has been picked: ', date);
            this.setState({
                chosenDate: moment(date).format('MMMM Do YYYY')
            })
            this._hideDateTimePicker();
        };
    
    
        _showDateTimePicker2 = () => this.setState({ isTimeVisible: true });
    
        _hideDateTimePicker2 = () => this.setState({ isTimeVisible: false });
    
        _handleDatePicked2 = (time) => {
            console.log('A date has been picked: ', time);
            this.setState({
                chosenTime: moment(time).format('H HH')
            })
            this._hideDateTimePicker2();
        };
    
        _showDateTimePicker3 = () => this.setState({ isFollowUpVisible: true });
    
        _hideDateTimePicker3 = () => this.setState({ isFollowUpVisible: false });
    
        _handleDatePicked3 = (datetime) => {
            console.log('A date has been picked: ', datetime);
            this.setState({
                chosenfollowupBy: moment(datetime).format('MMMM Do YYYY HH:mm')
            })
            this._hideDateTimePicker3();
        };

        _showDateTimePicker4 = () => this.setState({ isUpdateTillVisible: true });
    
        _hideDateTimePicker4 = () => this.setState({ isUpdateTillVisible: false });
    
        _handleDatePicked4 = (time) => {
            console.log('A date has been picked: ', time);
            this.setState({
                chosenUpdateTill: moment(time).format('H HH')
            })
            this._hideDateTimePicker4();
        };

        _showDateTimePicker5 = () => this.setState({ isMedTimeVisible: true });
    
        _hideDateTimePicker5 = () => this.setState({ isMedTimeVisible: false });
    
        _handleDatePicked5 = (time) => {
            console.log('A date has been picked: ', time);
            this.setState({
                chosenMedicineTime: moment(time).format('H HH')
            })
            this._hideDateTimePicker5();
        };
    
        saveData() {
            firebase.database().ref('Medications').push({
                date: this.state.chosenDate,
                time: this.state.chosenTime,
                title: this.state.title,
                medName : this.state.medicineName,
                medTime : this.state.chosenMedicineTime,
                quantity : this.state.quantity,
                repeat: this.state.repeat,
                updateTill : this.state.chosenUpdateTill,
                alarmType: this.state.alarmType,
                followupBy: this.state.chosenfollowupBy
            }).then(alert('Data Inserted'))
        }
    static navigationOptions = {
        header: null
    }
    render() {
        return (
            <ImageBackground
                source={require('./images/bg2.jpg')}
                style={{ flex: 1 }}
            >
                <View style={styles.container}>
                    <View style={styles.DateTimeContainer}>
                        <View style={styles.dateContainer}>
                        <Text style={{ color: 'red' }}>{this.state.chosenDate}</Text>
                            <TouchableOpacity
                                style={{ height: 45, borderBottomColor: 'rgba(255,255,255,0.8)', borderBottomWidth: 1 }}
                                onPress={this._showDateTimePicker}
                            >
                                <Text style={{ color: 'white', fontSize: 18 }}> Date </Text>

                            </TouchableOpacity>
                            <DateTimePicker
                                isVisible={this.state.isDateVisible}
                                onConfirm={this._handleDatePicked}
                                onCancel={this._hideDateTimePicker}
                                mode={'date'}
                            />
                        </View>
                        <View style={styles.timeContainer}>
                            <Text style={{ color: 'red' }}>{this.state.chosenTime}</Text>
                            <TouchableOpacity
                                style={{ height: 45, borderBottomColor: 'rgba(255,255,255,0.8)', borderBottomWidth: 1 }}
                                onPress={this._showDateTimePicker2}
                                underlineColorAndroid="rgba(255,255,255,0.8)"
                            >
                                <Text style={{ color: 'white', fontSize: 18 }}>Time</Text>

                            </TouchableOpacity>
                            <DateTimePicker
                                isVisible={this.state.isTimeVisible}
                                onConfirm={this._handleDatePicked2}
                                onCancel={this._hideDateTimePicker2}
                                mode={'time'}
                                is24Hour={false}
                            />
                        </View>
                    </View>


                    <View style={styles.inputs}>
                        <TextInput style={styles.input}
                            placeholder="Title"
                            onChangeText={(title)=>this.setState({title})}
                            placeholderTextColor="rgba(255,255,255,0.8)"
                            underlineColorAndroid="rgba(255,255,255,0.8)"
                            keyboardType="default"
                            autoCorrect={false}
                            returnKeyType="next"
                            ref={"Title"}
                            onSubmitEditing={() => this.refs.Medication.focus()}
                        />
                        <TextInput style={styles.input}
                            placeholder="Medication"
                            onChangeText={(medicineName)=>this.setState({medicineName})}
                            placeholderTextColor="rgba(255,255,255,0.8)"
                            underlineColorAndroid="rgba(255,255,255,0.8)"
                            keyboardType="default"
                            autoCorrect={false}
                            returnKeyType="next"
                            ref={"Medication"}
                        />
                        <View style={[styles.input2, {borderBottomColor: 'rgba(255,255,255,0.8)', borderBottomWidth: 1}]}>
                        <Text style={{ color: 'red' }}>{this.state.chosenMedicineTime}</Text>
                            <TouchableOpacity
                                style={{ height: 45 }}
                                onPress={this._showDateTimePicker5}
                                underlineColorAndroid="rgba(255,255,255,0.8)"
                            >
                                <Text style={{ color: 'white', fontSize: 18 }}>Time</Text>

                            </TouchableOpacity>
                            <DateTimePicker
                                isVisible={this.state.isMedTimeVisible}
                                onConfirm={this._handleDatePicked5}
                                onCancel={this._hideDateTimePicker5}
                                mode={'time'}
                                is24Hour={false}
                            />
                        </View>
                        <TextInput style={styles.input2}
                            placeholder="Repeat"
                            onChangeText={(repeat)=>this.setState({repeat})}
                            placeholderTextColor="rgba(255,255,255,0.8)"
                            underlineColorAndroid="rgba(255,255,255,0.8)"
                            keyboardType="default"
                            autoCorrect={false}
                            returnKeyType="next"
                            ref={"Repeat"}
                            onSubmitEditing={() => this.refs.Quantity.focus()}
                        />
                        <TextInput style={styles.input2}
                            placeholder="Quantity"
                            onChangeText={(quantity)=>this.setState({quantity})}
                            placeholderTextColor="rgba(255,255,255,0.8)"
                            underlineColorAndroid="rgba(255,255,255,0.8)"
                            keyboardType="default"
                            autoCorrect={false}
                            returnKeyType="next"
                            ref={"Quantity"}
                        />
                        <View style={[styles.input2, {borderBottomColor: 'rgba(255,255,255,0.8)', borderBottomWidth: 1}]}>
                            <Text style={{ color: 'red' }}>{this.state.chosenUpdateTill}</Text>
                            <TouchableOpacity
                                style={{ height: 45, borderBottomColor: 'rgba(255,255,255,0.8)', borderBottomWidth: 1 }}
                                onPress={this._showDateTimePicker4}
                                underlineColorAndroid="rgba(255,255,255,0.8)"
                            >
                                <Text style={{ color: 'white', fontSize: 18 }}>Update Till</Text>

                            </TouchableOpacity>
                            <DateTimePicker
                                isVisible={this.state.isUpdateTillVisible}
                                onConfirm={this._handleDatePicked4}
                                onCancel={this._hideDateTimePicker4}
                                mode={'time'}
                                is24Hour={false}
                            />
                        </View>
                        <TextInput style={styles.input}
                            placeholder="Alarm Type"
                            onChangeText={(alarmType)=>this.setState({alarmType})}
                            placeholderTextColor="rgba(255,255,255,0.8)"
                            underlineColorAndroid="rgba(255,255,255,0.8)"
                            keyboardType="default"
                            autoCorrect={false}
                            returnKeyType="next"
                            ref={"AlarmType"}
                        />
                        <View>
                            <Text style={{ color: 'red' }}>{this.state.chosenfollowupBy}</Text>
                            <TouchableOpacity
                                style={{ height: 45, borderBottomColor: 'rgba(255,255,255,0.8)', borderBottomWidth: 1 }}
                                onPress={this._showDateTimePicker3}
                            >
                                <Text style={{ color: 'white', fontSize: 19 }}>FollowupBy</Text>

                            </TouchableOpacity>
                            <DateTimePicker
                                isVisible={this.state.isFollowUpVisible}
                                onConfirm={this._handleDatePicked3}
                                onCancel={this._hideDateTimePicker3}
                                mode={'datetime'}
                            />
                        </View>
                        <View style={styles.ButtonContainer}>
                            <TouchableOpacity style={styles.buttonLogin}
                                onPress={()=> this.saveData()}
                            >
                                <Text style={styles.buttonText}>DONE</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.buttonRegister}
                                onPress={() => this.props.navigation.navigate('RegToMain')}
                            >
                                <Text style={styles.buttonText}>SHARE</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 50,
    },
    DateTimeContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },
    dateContainer: {
        flex: 1,
        marginRight: 10,
    },
    timeContainer: {
        flex: 1,
        marginLeft: 10,
    },
    inputs: {
        flex: 4,
        flexDirection: 'column',
        justifyContent: 'space-around'
    },
    Attachments: {
        marginTop: 20
    },
    buttonLogin: {
        backgroundColor: '#0e251a',
        height: 40,
        paddingVertical: 15,
        justifyContent: 'center',
        marginTop: 20
    },
    buttonText: {
        textAlign: 'center',
        color: 'white',
        fontWeight: 'bold',
        fontSize: 17,
        letterSpacing: 2
    },
    input: {
        height: 45,
        color: 'white',
        fontSize: 20,
    },
    input2: {
        height: 45,
        color: 'white',
        fontSize: 20,
        marginHorizontal: 30
    },
    ButtonContainer: {
        flexDirection: 'row',
    },
    buttonLogin: {
        flex: 5,
        backgroundColor: '#1d9d74',
        //backgroundColor: 'rgba(50,205,50, 0.5)',
        height: 40,
        paddingVertical: 15,
        justifyContent: 'center',
        marginRight: 10,
        borderRadius: 5,
    },
    buttonRegister: {
        flex: 5,
        backgroundColor: 'rgba(198, 40, 40,1.0)',
        //backgroundColor: 'rgba(50,205,50, 0.5)',
        height: 40,
        paddingVertical: 15,
        borderRadius: 5,
        justifyContent: 'center'
    },
    buttonText: {
        textAlign: 'center',
        color: 'white',
        fontWeight: 'bold',
        letterSpacing: 2,
        fontSize: 17
    },
})