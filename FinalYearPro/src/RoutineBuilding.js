import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    ImageBackground
}from 'react-native';

export default class RoutineBuilding extends Component {
    render() {
        return(
            <ImageBackground
                source={require('./images/bg2.jpg')}
                style={{flex:1}}
            >
            <View style= {styles.conatainer}>
                <Text style={{fontSize:17 , fontStyle:'italic', color: 'white'}}>Routine Not Set</Text>
            </View>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create ({
    conatainer : {
        flex: 1,
        justifyContent : 'center',
        alignItems : 'center'
    }
})