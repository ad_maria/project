import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    ImageBackground
} from 'react-native';

export default class BlockPeople extends Component {
    render() {
        return (
            <ImageBackground
                source={require('./images/bg2.jpg')}
                style={{ flex: 1 }}
            >
                <View style={styles.container}>
                    <Text style={{ fontSize: 17, fontStyle: 'italic', color: 'white' }}> No Contact Found</Text>
                </View>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        justifyContent: 'center',
        alignItems: 'center',
    }
})