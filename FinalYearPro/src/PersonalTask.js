import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    TextInput,
    ImageBackground
} from 'react-native';
import firebase from '@firebase/app'
import database from '@firebase/database';
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment';

/*const firebaseConfig = {
    apiKey: "AIzaSyDjsdQfhxuHXGsGEMXxxHgp0rzrgsIOyi8",
    authDomain: "authentication-d35ff.firebaseapp.com",
    databaseURL: "https://authentication-d35ff.firebaseio.com",
    projectId: "authentication-d35ff",
    storageBucket: "authentication-d35ff.appspot.com",
    messagingSenderId: "280481447201"
}
const firebaseApp = firebase.initializeApp(firebaseConfig);*/

export default class PersonalTask extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            chosenDate: '',
            chosenTime: '',
            title: '',
            repeat: '',
            alarmType: '',
            participants: '',
            chosenfollowupBy: '',
            attachment: '',
            taskCategory: '',

            isDateVisible: false,
            isTimeVisible: false,
            isFollowUpVisible: false

        })
    }


    static navigationOptions = {
        header: null
    }

    
    _showDateTimePicker = () => this.setState({ isDateVisible: true });

    _hideDateTimePicker = () => this.setState({ isDateVisible: false });

    _handleDatePicked = (date) => {
        console.log('A date has been picked: ', date);
        this.setState({
            chosenDate: moment(date).format('MMMM Do YYYY')
        })
        this._hideDateTimePicker();
    };


    _showDateTimePicker2 = () => this.setState({ isTimeVisible: true });

    _hideDateTimePicker2 = () => this.setState({ isTimeVisible: false });

    _handleDatePicked2 = (time) => {
        console.log('A date has been picked: ', time);
        this.setState({
            chosenTime: moment(time).format('H HH')
        })
        this._hideDateTimePicker2();
    };

    _showDateTimePicker3 = () => this.setState({ isFollowUpVisible: true });

    _hideDateTimePicker3 = () => this.setState({ isFollowUpVisible: false });

    _handleDatePicked3 = (datetime) => {
        console.log('A date has been picked: ', datetime);
        this.setState({
            chosenfollowupBy: moment(datetime).format('MMMM Do YYYY HH:mm')
        })
        this._hideDateTimePicker3();
    };

    saveData() {
        firebase.database().ref('PersonalTasks').push({
            date: this.state.chosenDate,
            time: this.state.chosenTime,
            title: this.state.title,
            repeat: this.state.repeat,
            alarmType: this.state.alarmType,
            participants: this.state.participants,
            followupBy: this.state.chosenfollowupBy
        }).then(alert('Data Saved !'))
    }

    render() {
        return (
            <ImageBackground
                source={require('./images/bg2.jpg')}
                style={{ flex: 1 }}
            >
                <View style={styles.container}>
                    <View style={styles.DateTimeContainer}>
                        <View style={styles.dateContainer}>
                            <Text style={{ color: 'red' }}>{this.state.chosenDate}</Text>
                            <TouchableOpacity
                                style={{ height: 45, borderBottomColor: 'rgba(255,255,255,0.8)', borderBottomWidth: 1 }}
                                onPress={this._showDateTimePicker}
                            >
                                <Text style={{ color: 'white', fontSize: 18 }}> Date </Text>

                            </TouchableOpacity>
                            <DateTimePicker
                                isVisible={this.state.isDateVisible}
                                onConfirm={this._handleDatePicked}
                                onCancel={this._hideDateTimePicker}
                                mode={'date'}
                            />
                        </View>
                        <View style={styles.timeContainer}>
                            <Text style={{ color: 'red' }}>{this.state.chosenTime}</Text>
                            <TouchableOpacity
                                style={{ height: 45, borderBottomColor: 'rgba(255,255,255,0.8)', borderBottomWidth: 1 }}
                                onPress={this._showDateTimePicker2}
                                underlineColorAndroid="rgba(255,255,255,0.8)"
                            >
                                <Text style={{ color: 'white', fontSize: 18 }}>Time</Text>

                            </TouchableOpacity>
                            <DateTimePicker
                                isVisible={this.state.isTimeVisible}
                                onConfirm={this._handleDatePicked2}
                                onCancel={this._hideDateTimePicker2}
                                mode={'time'}
                                is24Hour={false}
                            />
                        </View>
                    </View>


                    <View style={styles.inputs}>
                        <TextInput style={styles.input}
                            placeholder="Title"
                            placeholderTextColor="rgba(255,255,255,0.8)"
                            underlineColorAndroid="rgba(255,255,255,0.8)"
                            keyboardType="default"
                            autoCorrect={false}
                            returnKeyType="next"
                            ref={"Title"}
                            onSubmitEditing={() => this.refs.Repeat.focus()}
                            onChangeText={(title) => this.setState({ title })}
                            value={this.state.title}
                        />
                        <TextInput style={styles.input}
                            placeholder="Repeat"
                            underlineColorAndroid="rgba(255,255,255,0.8)"
                            placeholderTextColor="rgba(255,255,255,0.8)"
                            keyboardType="default"
                            autoCorrect={false}
                            returnKeyType="next"
                            ref={"Repeat"}
                            onSubmitEditing={() => this.refs.AlarmType.focus()}
                            onChangeText={(repeat) => this.setState({ repeat })}
                            value={this.state.repeat}
                        />
                        <TextInput style={styles.input}
                            placeholder="AlarmType"
                            underlineColorAndroid="rgba(255,255,255,0.8)"
                            placeholderTextColor="rgba(255,255,255,0.8)"
                            keyboardType="default"
                            autoCorrect={false}
                            returnKeyType="next"
                            ref={"AlarmType"}
                            onSubmitEditing={() => this.refs.Participants.focus()}
                            onChangeText={(alarmType) => this.setState({ alarmType })}
                            value={this.state.alarmType}
                        />
                        <TextInput style={styles.input}
                            placeholder="Participants"
                            underlineColorAndroid="rgba(255,255,255,0.8)"
                            placeholderTextColor="rgba(255,255,255,0.8)"
                            keyboardType="default"
                            autoCorrect={false}
                            returnKeyType="next"
                            ref={"Participants"}
                            onChangeText={(participants) => this.setState({ participants })}
                            value={this.state.participants}
                        />
                        <View>
                            <Text style={{ color: 'red' }}>{this.state.chosenfollowupBy}</Text>
                            <TouchableOpacity
                                style={{ height: 45, borderBottomColor: 'rgba(255,255,255,0.8)', borderBottomWidth: 1 }}
                                onPress={this._showDateTimePicker3}
                            >
                                <Text style={{ color: 'white', fontSize: 19 }}>FollowupBy</Text>

                            </TouchableOpacity>
                            <DateTimePicker
                                isVisible={this.state.isFollowUpVisible}
                                onConfirm={this._handleDatePicked3}
                                onCancel={this._hideDateTimePicker3}
                                mode={'datetime'}
                            />
                        </View>
                        <TouchableOpacity style={styles.buttonLogin}
                            onPress={() => this.saveData()}
                        >
                            <Text style={styles.buttonText}>DONE</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 50,
    },
    DateTimeContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-end',
        paddingBottom: 25,
    },
    dateContainer: {
        flex: 1,
        marginRight: 10,
    },
    timeContainer: {
        flex: 1,
        marginLeft: 10,
    },
    inputs: {
        flex: 4,
        flexDirection: 'column',
        justifyContent: 'space-around'
    },
    Attachments: {
        marginTop: 20
    },
    buttonLogin: {
        backgroundColor: '#1d9d74',
        borderRadius: 5,
        height: 40,
        paddingVertical: 15,
        justifyContent: 'center',
        marginTop: 20
    },
    buttonText: {
        textAlign: 'center',
        color: 'white',
        fontWeight: 'bold',
        fontSize: 17,
        letterSpacing: 2
    },
    input: {
        height: 45,
        color: 'white',
        fontSize: 20,
    }
})