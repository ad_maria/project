import React, { Component } from 'react';
import {
    View,
    Text,
    TextInput,
    StyleSheet,
    StatusBar,
    TouchableOpacity,
    Alert,
    Image,
    ImageBackground
} from 'react-native';
import firebase from '@firebase/app';
import auth from '@firebase/auth';

export default class Login extends Component {

    constructor(props) {
        super(props)
        this.state = ({
            email: '',
            password: '',
            errorMessage: null
        })
    }

    
    static navigationOptions = {
        header: null
    }

    loginUser = (email, password) => {
        firebase.auth().signInWithEmailAndPassword(email, password)
            .then(() => this.props.navigation.navigate('MainScreen'))
            .catch(error => Alert.alert(error.toString()))

    }
    render() {
        return (
            <ImageBackground 
                source={require('./images/bg2.jpg')}
                style={styles.container}
            >
                <StatusBar barStyle="light-content" />

                <View style={styles.container}>
                    
                    <View style={styles.logoContainer}>
                        <Image
                            style={{ width: 200, height: 200, borderRadius:10 , borderColor: '#1d9d74', borderWidth: 3 }}
                            source={require('./images/image1.png')}
                        />
                    </View>
                    <View style={styles.loginInputsCon}>
                        <TextInput style={styles.input}
                            onChangeText={(email) => this.setState({ email })}
                            value={this.state.email}
                            placeholder="Email"
                            placeholderTextColor="rgba(255,255,255,0.8)"
                            keyboardType="email-address"
                            returnKeyType="next"
                            autoCorrect={false}
                            underlineColorAndroid="rgba(255,255,255,0.8)"
                            onSubmitEditing={() => this.refs.textPassword.focus()}
                        />
                        <TextInput style={styles.input}
                            onChangeText={(password) => this.setState({ password })}
                            value={this.state.password}
                            placeholder="Password"
                            placeholderTextColor="rgba(255,255,255,0.8)"
                            returnKeyType="go"
                            autoCorrect={false}
                            secureTextEntry
                            underlineColorAndroid="rgba(255,255,255,0.8)"
                            ref={"textPassword"}
                        />
                        <View style={styles.ButtonContainer}>
                            <TouchableOpacity style={styles.buttonLogin}
                                onPress={() => this.loginUser(this.state.email, this.state.password)}
                            >
                                <Text style={styles.buttonText}>LOGIN</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.buttonRegister}
                                onPress={() => this.props.navigation.navigate('RegMain')}
                            //onPress={()=>this.SignUpUser(this.state.email , this.state.password)}
                            >
                                <Text style={styles.buttonText}>REGISTER</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
    },
    logoContainer: {
        flex: 7,
        alignItems: 'center',
        justifyContent: 'center'
    },
    loginInputsCon: {
        flex: 3,
        justifyContent: 'center',
        padding: 25,
        marginBottom: 10
    },
    logo: {
        fontSize: 35,
        color: 'black',
        //color: 'rgba(50,205,50, 0.5)',
        opacity: 0.9
    },
    input: {
        height: 40,
        color: 'white',
        fontSize: 17,
        marginVertical: 15,
        paddingHorizontal: 10,
    },
    buttonLogin: {
        flex: 5,
        backgroundColor: '#1d9d74',
        borderRadius : 5,
        //backgroundColor: 'rgba(50,205,50, 0.5)',
        height: 40,
        paddingVertical: 15,
        justifyContent: 'center',
        marginRight: 10
    },
    buttonRegister: {
        flex: 5,
        backgroundColor: 'rgba(198, 40, 40,1.0)',
        borderRadius: 5,
        //backgroundColor: 'rgba(50,205,50, 0.5)',
        height: 40,
        paddingVertical: 15,
        justifyContent: 'center'
    },
    buttonText: {
        textAlign: 'center',
        color: 'white',
        fontWeight: 'bold',
        letterSpacing: 2,
        fontSize: 17
    },
    ButtonContainer: {
        flex: 1,
        flexDirection: 'row',
    }


})