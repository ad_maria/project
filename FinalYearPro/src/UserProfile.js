import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    TextInput,
    TouchableOpacity,
    ImageBackground
} from 'react-native';

export default class UserProfile extends Component {
    render() {
        return (
            <ImageBackground 
                source={require('./images/bg3.png')}
                style={{flex : 1}}
            >
            <View style={styles.container}>
                <View style={styles.TopContainer}>
                    <View style={styles.ImageContainer}>
                        <Image
                            style={{ width: 150, height: 150, borderRadius: 250 / 2, borderColor: '#1d9d74', borderWidth: 3 }}
                            source = {require('./images/defaultUser.png')}
                        />
                    </View>
                    <View style={styles.userStyle}>
                        <TextInput style={styles.input}
                            placeholder="UserName"
                            placeholderTextColor="rgba(255,255,255,0.8)"
                            keyboardType="email-address"
                            autoCorrect={false}
                            returnKeyType="next"
                            underlineColorAndroid=""
                            onSubmitEditing={() => this.refs.Email.focus()}
                        />
                    </View>
                </View>

                <View style={styles.inputs}>
                    <TextInput style={styles.input}
                        placeholder="Email"
                        placeholderTextColor="rgba(255,255,255,0.8)"
                        keyboardType="email-address"
                        returnKeyType="next"
                        autoCorrect={false}
                        underlineColorAndroid=""
                        ref={"Email"}
                        onSubmitEditing={() => this.refs.Number.focus()}
                    />
                    <TextInput style={styles.input}
                        placeholder="Number"
                        placeholderTextColor="rgba(255,255,255,0.8)"
                        keyboardType="numeric"
                        returnKeyType="next"
                        autoCorrect={false}
                        underlineColorAndroid=""
                        ref={"Number"}
                        onSubmitEditing={() => this.refs.Residence.focus()}
                    />
                    <TextInput style={styles.input}
                        placeholder="Residence"
                        placeholderTextColor="rgba(255,255,255,0.8)"
                        keyboardType="default"
                        returnKeyType="next"
                        autoCorrect={false}
                        underlineColorAndroid=""
                        ref={"Residence"}
                        onSubmitEditing={() => this.refs.Organization.focus()}
                    />
                    <TextInput style={styles.input}
                        placeholder="Organization"
                        placeholderTextColor="rgba(255,255,255,0.8)"
                        keyboardType="default"
                        returnKeyType="next"
                        autoCorrect={false}
                        underlineColorAndroid=""
                        ref={"Organization"}
                        onSubmitEditing={() => this.refs.OrgLocation.focus()}
                    />
                    <TextInput style={styles.input}
                        placeholder="Organization Location"
                        placeholderTextColor="rgba(255,255,255,0.8)"
                        keyboardType="default"
                        returnKeyType="next"
                        autoCorrect={false}
                        underlineColorAndroid=""
                        ref={"OrgLocation"}
                        onSubmitEditing={() => this.refs.Interest.focus()}
                    />
                    <TextInput style={styles.input}
                        placeholder="Interests"
                        placeholderTextColor="rgba(255,255,255,0.8)"
                        keyboardType="default"
                        returnKeyType="go"
                        autoCorrect={false}
                        underlineColorAndroid=""
                        ref={"Interest"}
                        onSubmitEditing={() => this.refs.textPassword.focus()}
                    />

                     <TouchableOpacity style={styles.buttonLogin}>
                                <Text style={styles.buttonText}>SAVE CHANGES</Text>
                            </TouchableOpacity>
                    </View>


            </View >
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 50,
    },
    TopContainer: {
        flex: 1,
        flexDirection: 'row',
    },
    ImageContainer: {
        flex: 1
    },
    userStyle: {
        flex: 1,
        alignSelf: 'center',
        justifyContent: 'space-around'
    },
    inputs: {
        flex: 3.5,
        justifyContent: 'flex-end'
    },
    buttonLogin : {
        backgroundColor : '#1d9d74',
        borderRadius:5,
        height: 40 ,
        marginTop: 10,
        paddingVertical : 15,
        justifyContent : 'center',
        marginBottom: 20
    },
    buttonText : {
        textAlign : 'center',
        color : 'white',
        fontWeight : 'bold',
        fontSize: 17,
        letterSpacing: 2
    }
})