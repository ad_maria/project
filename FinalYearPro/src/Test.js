import React, {Component} from 'react';
import {createMaterialTopTabNavigator} from 'react-navigation';
import RoutineBuilding from './RoutineBuilding'
import OnGoingTask from './OngoingTasks';


export default class Main extends Component {
    static navigationOptions = {
        header : null
    }
    render() {
        return (
            <AppNavigator />
        );
    }
}

const AppNavigator = createMaterialTopTabNavigator ({

    Task : OnGoingTask,
    RoutBuild : RoutineBuilding
    
})