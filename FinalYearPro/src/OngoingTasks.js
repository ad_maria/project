import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    ImageBackground,
    ListView,
} from 'react-native';
import firebase from '@firebase/app';
import database from '@firebase/database';



export default class OngoingTasks extends Component {
    constructor() {
        super();
        let ds = new ListView.DataSource({rowHasChanged : (r1,r2) => r1 !==r2});
        this.state = {
            itemDataSource : ds
        } 
        this.itemsRef = this.getRef().child('PersonalTasks')
        this.renderRow = this.renderRow.bind(this);
        this.pressRow = this.pressRow.bind(this);
    }
    static navigationOptions = {
        header: null
    }

    componentWillMount() {
        this.getItems(this.itemsRef);
    }

    componentDidMount() {
        this.getItems(this.itemsRef);
    }

    getItems(itemsRef) {
        //let items = [{title : 'Item One '} , {title : 'Item Two'}];

        itemsRef.on('value',(snap)=> {
            let items =[];
            snap.forEach((child) => {
                items.push({
                    title: child.val().title,
                    _key : child.key,
                    followupBy : child.val().followupBy
                })
            })
            this.setState({
                itemDataSource : this.state.itemDataSource.cloneWithRows(items)
            })
        });

    }

    getRef() {
        return firebase.database().ref();
    }

    pressRow(item) {
        console.log(item);
    }

    renderRow(item){
        return(
            <TouchableOpacity
                onPress={()=> {
                    this.pressRow(item);
                }}
            >   
                <View style={styles.li}>
                    <Text style={[styles.liText, {fontSize: 20}]}>{item.title}</Text>
                    <Text style={[styles.liText, {color:'red', fontSize:14}]}>{item.followupBy}</Text>
                </View>
            </TouchableOpacity>
        )
    }

    render() {
        return (
            <ImageBackground
                source={require('./images/bg2.jpg')}
                style={styles.container}
            >
                <View style={styles.ShowOngoingTasks}>
                    <ListView 
                        dataSource = {this.state.itemDataSource}
                        renderRow = {this.renderRow}
                    />
                </View>
                <View style={styles.buttonView}>
                    <TouchableOpacity
                        style={styles.createTaskButton}
                        //onPress={() => this.props.navigation.navigate('createInd')}
                        onPress = { () => alert('ALHAMDULILLAH')}
                    >
                        <Text style={styles.ButtonText}>CREATE TASK</Text>
                    </TouchableOpacity>
                </View>

            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },

    ShowOngoingTasks: {
        flex: 4,
        //backgroundColor:'#f2f2f2'
    },
    listview : {
        flex : 1
    },
    li : {
        backgroundColor :'#fff',
        borderBottomColor: '#eee',
        borderColor: 'transparent',
        borderWidth: 1,
        paddingLeft: 16,
        paddingTop: 14,
        paddingBottom: 16,
    },
    liText : {
        color: '#333',
        fontSize : 16
    },
    buttonView: {
        flex: 0.5,
        justifyContent: 'center',
        paddingHorizontal: 50,
    },
    createTaskButton: {
        backgroundColor: '#1d9d74',
        padding: 10,
        borderRadius: 5,
    },
    ButtonText: {
        fontSize: 15,
        fontWeight: 'bold',
        color: 'white',
        alignSelf: 'center',
    }
})