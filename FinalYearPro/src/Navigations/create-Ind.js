
import React, { Component } from 'react';
import {createStackNavigator} from 'react-navigation';

import createTask from '../createTask'; 
import PersonalTask from '../PersonalTask';
import ShareTask from '../ShareTask';
import Habit from '../Habit'
import Medication from '../Medication';
import Gathering from '../Gathering';

export default class App extends Component {
  static navigationOptions = {
    headerStyle : {
      backgroundColor: '#1d9d74',
    }
  }
  render() {
    return (
      < AppNavigator />
    );
  }
}

const AppNavigator =  createStackNavigator({
  
  createTask : createTask,
  PersonalTask : PersonalTask,
  ShareTask : ShareTask,
  Medication : Medication,
  Habit : Habit,
  Gathering : Gathering

})