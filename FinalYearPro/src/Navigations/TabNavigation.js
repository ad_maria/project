import React, {Component} from 'react';
import {createMaterialTopTabNavigator} from 'react-navigation';
import RoutineBuilding from '../RoutineBuilding'
import OnGoingTask from '../OngoingTasks';


export default class Main extends Component {
    static navigationOptions = {
        header : null
    }
    render() {
        return (
            <AppNavigator />
        );
    }
}

const AppNavigator = createMaterialTopTabNavigator ({

    Task : { 
        screen : OnGoingTask ,
        navigationOptions : {
            tabBarLabel : 'ONGOING TASKS'
        }
    },
    RoutineBuild : { 
        screen : RoutineBuilding,
        navigationOptions : {
            tabBarLabel : 'ROUTINE BUILDING'
        }
    },   
},{
    initialRouteName : 'Task',
        tabBarOptions : {
            activeTintColor : 'white',
            inactiveTintColor : 'white',
            labelStyle : {
                fontSize : 17,
                fontWeight : '700'
            },
            style : {
                backgroundColor : '#1d9d74',
                borderTopColor: 'white',
                borderTopWidth : 1
            },
            indicatorStyle : {
                height : 3,
                backgroundColor : 'red'
            }
        }
    
})