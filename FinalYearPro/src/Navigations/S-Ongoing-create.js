
import React, { Component } from 'react';
import { createStackNavigator } from 'react-navigation';
import firebase from 'firebase';

import OngoingTasks from '../OngoingTasks';
//import MainPage from './TabNavigation'
import createTask from '../createTask';
import createInd from './create-Ind';

export default class App extends Component {

    componentWillMount() {
      const firebaseConfig = {
        apiKey: "AIzaSyDjsdQfhxuHXGsGEMXxxHgp0rzrgsIOyi8",
        authDomain: "authentication-d35ff.firebaseapp.com",
        databaseURL: "https://authentication-d35ff.firebaseio.com",
        projectId: "authentication-d35ff",
        storageBucket: "authentication-d35ff.appspot.com",
        messagingSenderId: "280481447201"
      };
      if (!firebase.apps.length) {
        firebase.initializeApp(firebaseConfig);
      }
  }
  render() {
    return (
      < AppNavigator />
    );
  }
}

const AppNavigator = createStackNavigator({

  OngoingTasks: OngoingTasks,
  createInd: createInd
})