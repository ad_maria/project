
import React, { Component } from 'react';
import { createStackNavigator } from 'react-navigation';

import Drawer from './DrawerNavigation';
import OngoingTasks from '../OngoingTasks';
import createInd from './create-Ind';

export default class App extends Component {

  static navigationOptions = {
    header: null
  }
  render() {
    return (
        < AppNavigator />
    );
  }
}

const AppNavigator = createStackNavigator({

  Drawer: Drawer,
  createInd: createInd,
  OngoingTasks: OngoingTasks,
})