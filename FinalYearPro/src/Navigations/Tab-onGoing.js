
import React, { Component } from 'react';
import { createStackNavigator } from 'react-navigation';

import MainPage from './TabNavigation'
import OngoingTasks from '../OngoingTasks';

export default class App extends Component {
 static navigationOptions ={
     header : null
 }
  render() {
    return (
      < AppNavigator />
    );
  }
}

const AppNavigator = createStackNavigator({

  MainPage: MainPage,
  OngoingTasks : OngoingTasks,
})