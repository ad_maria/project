
import React, { Component } from 'react';
import {createStackNavigator} from 'react-navigation';

import Drawer from './DrawerNavigation'
import Register from '../Register';


export default class App extends Component {
  static navigationOptions = {
    header : null
}
  render() {
    return (
      < AppNavigator />
    );
  }
}

const AppNavigator =  createStackNavigator({
    
    Register : Register,
    Drawer : Drawer
})