import React, { Component } from 'react';
import {
    SafeAreaView,
    View,
    ScrollView,
    Image
} from 'react-native';
import {createDrawerNavigator , DrawerItems} from 'react-navigation';

import Task from './TabNavigation';
import UserProfile from '../UserProfile';
import ShowSentTasks from '../ShowSentTasks';
import ShowGatherings from '../ShowGatherings';
import InvitePeople from '../InvitePeople';
import BlockPeople from '../BlockPeople';


export default class App extends Component {
    static navigationOptions = {
        header : null
    }
  render() {
    return (
        <AppNavigator />
    );
  }
}

const CustomDrawerComponent = (props) => (
    <SafeAreaView style={{flex :1}}>
        <View style={{height:200, backgroundColor : 'white' , alignItems: 'center' , justifyContent:'center'}}>
            <Image source={require('../images/image1.png')}
                style={{height:120 , width : 120 , borderRadius : 60}}
            />
        </View>
        <ScrollView>
            <DrawerItems {...props} 
                //activeTintColor = '#0e251a'
                activeTintColor = 'white'
                activeBackgroundColor ='#1d9d74'
                inactiveTintColor = 'black'
            />
        </ScrollView>
    </SafeAreaView>
)

const AppNavigator =  createDrawerNavigator({
    Task : Task,
    UserProfile : UserProfile,
    ShowSentTasks : ShowSentTasks ,
    ShowGatherings : ShowGatherings,
    InvitePeople : InvitePeople ,
    BlockPeople : BlockPeople,
},{
    initialRouteName: 'Task',
    contentComponent : CustomDrawerComponent,
})

